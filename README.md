
# laravel-testing
  

## Description

Testing utils for Laravel
  

## Depencencies

* none
  

## Installation
  

**In your composer.json**

Add the following code to the "repositories" key:

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://gitlab.com/schlabs/laravel/laravel-testing"
	}
]
```

Add this line to your require dependecies:

```json
"schlabs/laravel-testing": "1.0.*"
```
 
**In your project root run:**
```sh
composer update
```

## Usage

1. Include the "Illuminate\Foundation\Testing\RefreshDatabase" trait in your test case
2. Include the "SchLabs\LaravelTesting\Traits\UseDatabaseTesting" trait in your test case.
3. You can change the name of the testing database by creating a new variable in your test case, named $testingDatabaseName (by default uses "sqlite_testing")


