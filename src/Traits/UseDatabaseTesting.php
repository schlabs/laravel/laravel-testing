<?php
namespace SchLabs\LaravelTesting\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\ParallelTesting;

/**
 * Use this trait in your tests cases that use RefreshDatabase
 * It forces to use the database named in $testingDatabaseName
 */
trait UseDatabaseTesting
{
    private string $defaultDatabaseName;
    private string $testingDatabaseName = 'sqlite_testing';

    /**
     * Set the database to testing database and calls the init function (if exists)
     */
    public function setUp(): void
    {
        $this->parentSetUp();

        if(method_exists(get_called_class(), 'init')){
            $this->init();
        }
    }

    /**
     * @override of  \Illuminate\Foundation\Testing\TestCase:setUp();
     */
    private function parentSetUp()
    {
        Facade::clearResolvedInstances();

        if (! $this->app) {
            $this->refreshApplication();

            ParallelTesting::callSetUpTestCaseCallbacks($this);
        }

        //Changing the default database to testing
        $this->defaultDatabaseName = Config::get('database.default');
        Config::set('database.default', $this->testingDatabaseName);

        $this->setUpTraits();

        foreach ($this->afterApplicationCreatedCallbacks as $callback) {
            $callback();
        }

        Model::setEventDispatcher($this->app['events']);
        $this->setUpHasRun = true;
    }

    /**
     * Restore database config to previous database
     */
    public function tearDown(): void
    {
        //Restoring the default database to the previous configured
        Config::set('database.default', $this->defaultDatabaseName);

        parent::tearDown();
    }
}
